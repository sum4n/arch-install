# ARCH LINUX INSTALL INSTRUCTION using [ LVM & BTRFS & EXT4 ]. ONLY FOR UEFI SYSTEM

### Wireless connection
```bash
setfont ter-132b #first give your font a big size
iwctl
device list
station "your-device" scan
station "your-device" get-networks
station "your-device" connect "ESSID"
# Or find out the station name using (ip link) and if you know the ESSID of the wifi connection then run the following commands
iwctl --passphrase <password-here> station <station-here> connect <ESSID-here>
```

### Enable parallel downloads
* Edit `/etc/pacman.conf` file uncomment `ParallelDownloads = 5`

### Choose your country mirrors
```bash
pacman -Sy reflector
cp -r /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.old
reflector --protocol https --verbose --country "your-country" -l 10 --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syy
```

### Set time
```bash
timedatectl set-ntp true
timedatectl status
```

### Set keymap
```bash
localectl list-keymaps | grep "your-keymap" # for those who know their keymap
localectl list-keymaps | less # list all keymap and Choose yours
loadkeys "your-keymap"
```

## Setup Filesystem

### See and choose the file partition
```bash
lsblk # list filesystem
cfdisk /dev/"your-partition" # easiest method
# if you creating a lvm partition then for the root-part choose filesystem [ linux lvm ]
```

### Make filesystem [ext4]
* **Boot file**
    ```bash
    mkfs.fat -F32 -n "VOL-NAME" /dev/"efi-part"
    ```

* **Root file**
    ```bash
    mkfs.ext4 -L "vol-name" /dev/"root-part"
    ```

* **Home file**
    ```bash
    mkfs.ext4 -L "vol-name" /dev/"home-part" # if you created seperate home partition
    ```

* **Swap file**
    ```bash
    mkswap /dev/"swap-part"
    swapon /dev/"swap-part"
    ```

### Mount file system [ext4]
* **First create needed directories**
    ```bash
    mkdir -p /mnt/{boot,home}
    ```

* **Now mount**
    ```bash
    mount /dev/"root-part" /mnt
    mount /dev/"home-part" /mnt/home # if you have home-part
    mount /dev/"efi-part" /mnt/boot
    ```

### Make filesystem [btrfs]
* **Boot file**
    ```bash
    mkfs.fat -F32 -n "VOL-NAME" /dev/"efi-part"
    ```

* **Root file**
    ```bash
    mkfs.btrfs -L "vol-name" /dev/"root-part"
    ```

* **Subvolume create process for [ btrfs ] filesystem**
    ```bash
    mount /dev/"root-part" /mnt
    cd /mnt
    btrfs su cr @
    btrfs su cr @home
    btrfs su cr @var
    btrfs su cr @tmp
    btrfs su cr @snapshots
    ```

### Mount filesystem [btrfs]
```bash
cd && umount /mnt
mkdir -p /mnt/{boot,home,var,tmp,.snapshots} # create directories
mount -o noatime,compress=lzo,ssd,space_cache,subvol=@ /dev/"root-part" /mnt
mount -o noatime,compress=lzo,ssd,space_cache,subvol=@home /dev/"root-part" /mnt/home
mount -o noatime,compress=lzo,ssd,space_cache,subvol=@var /dev/"root-part" /mnt/var
mount -o noatime,compress=lzo,ssd,space_cache,subvol=@tmp /dev/"root-part" /mnt/tmp
mount -o noatime,compress=lzo,ssd,space_cache,subvol=@snapshots /dev/"root-part" /mnt/.snapshots

# efi-part
mount /dev/"efi-part" /mnt/boot
```

### Make filesystem [lvm]
* **Boot file**
    ```bash
    mkfs.fat -F32 -n "VOL-NAME" /dev/"efi-part"
    ```

* **Lvm Volumes**
    ```bash
    pvcreate /dev/"root-part"
    vgcreate vg1 /dev/"root-part"
    pvdisplay # for display information
    ```

* **Root and Home file**
    ```bash
    lvcreate -L "size"GB vg1 -n "vol-name" # root-vol
    lvcreate -l 100%FREE vg1 -n "vol-name" # home-vol
    modprobe dm_mod
    vgscan # for scan created group
    vgchange -ay

    # format
    mkfs.ext4 /dev/vg1/"vol-name" # root-vol
    mkfs.ext4 /dev/vg1/"vol-name" # home-vol
    ```

### Mount filesystem [lvm]
```bash
mkdir -p /mnt/{home,boot}
mount /dev/"efi-part" /mnt/boot
mount /dev/vg1/"vol-name" /mnt # root-vol
mount /dev/vg1/"vol-name" /mnt/home # home-vol
```

### Install base system
```bash
pacstrap -i /mnt base vim lvm2 btrfs-progs base-devel linux linux-firmware linux-headers # regular linux kernel, if you dont have btrfs/lvm then remove [ btrfs-progs/lvm2 ] package
pacstrap -i /mnt base vim lvm2 btrfs-progs base-devel linux-lts linux-firmware linux-lts-headers # linux-lts kernel
```

### Generate fstab file
```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

### Copy the mirror and pacman config to chroot
```bash
# backup old
cp -r /mnt/etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist.old
cp -r /mnt/etc/pacman.conf /mnt/etc/pacman.conf.old

# copy existed
cp -r /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
cp -r /etc/pacman.conf /mnt/etc/pacman.conf
```

### <a name="chroot"></a>Change Root into system
```bash
arch-chroot /mnt
```

### BTRFS & LVM create a swap file
```bash
fallocate -l 2GB /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
cp -r /etc/fstab /etc/fstab.old
echo '/swapfile none swap defaults 0 0' | tee -a /etc/fstab
```

### Set TimeZone
```bash
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
hwclock --systohc --utc
```

### Generate Keymap
```bash
echo "KEYMAP=us" >> /etc/vconsole.conf # instead of [ us ] use your keymap
```

### Generate Locale
```bash
vim /etc/locale.gen # uncomment your locale
echo "LANG=en_US.UTF-8" >> /etc/locale.conf # instead of [ en_US.UTF-8 ] use your locale
locale-gen
```

### ! IMPORTANT: add these lines to your hosts file in next step
```bash
127.0.0.1   localhost
::1         localhost
127.0.1.1   "hostname".localdomain  "hostname"
```

### Edit your HostName and Hosts
```bash
echo "hostname" >> /etc/hostname
vim /etc/hosts
```

### Create user and password
```bash
passwd # for root
useradd -mG audio,video,optical,storage "username"
usermod -aG "username" "username"
passwd "username"
```

### Edit sudoers file
```bash
echo "username ALL=(ALL) ALL" >> /etc/sudoers.d/"username"
```

### Install Grub
```bash
pacman -S grub efibootmgr dosfstools os-prober mtools # if dualboot is not required then remove [ os-prober ]
mkdir /boot/EFI
mount /dev/"efi-part" /boot/EFI
grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=grub_uefi --recheck
grub-mkconfig -o /boot/grub/grub.cfg
```

### <a name="mkinit"></a>! IMPORTANT: Edit mkinitcpio file
```bash
# BTRFS
vim /etc/mkinitcpio.conf # look-for MODULES=() and change it to MODULES=(btrfs). If you have btrfs
# NVIDIA/AMD
vim /etc/mkinitcpio.conf # look-for MODULES=() change to MODULES=(nvidia or amdgpu). If you have one of those
# LVM
vim /etc/mkinitcpio.conf # look-for HOOKS=(.. block filesystems ..) change to HOOKS=(.. block lvm2 filesystems ..)

mkinitcpio -P linux-lts # linux-lts kernel
mkinitcpio -P linux # regular linux
```

### Install Needed Packages
```bash
pacman -S networkmanager git curl openssh wpa_supplicant dialog wireless_tools netctl upower ufw acpi acpid zsh avahi xdg-utils xdg-user-dirs mesa terminus-font # must install packages for base
pacman -S bluez bluez-utils blueman # for bluetooth
pacman -S pulseaudio pulseaudio-bluetooth pulsemixer pavucontrol alsa-firmware alsa-lib alsa-plugins # for sound
pacman -S xorg xorg-server xorg-xinit lightdm lightdm-gtk-greeter-settings lightdm-gtk-greeter xfce4 xfce4-goodies file-roller # for xfce desktop
pacman -S amd-ucode xf86-video-amdgpu # for amd cpu
pacman -S intel-ucode xf86-video-intel # for intel cpu
pacman -S nvidia nvidia-lts nvidia-utils nvidia-settings # for nvidia gpu
```

### Enable services
```bash
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable sshd
systemctl enable upower
systemctl enable ufw
systemctl enable lightdm
systemctl enable acpid
```

# If you want to setup via script then, After [arch-chroot](#chroot) follow these steps
```bash
git clone https://gitlab.com/sum4n/arch-install
cd arch-install
chmod +x initial-setup.sh
./initial-setup.sh
```

## ! IMPORTANT: when the script is finished successfully, repeat the [mkinitcpio config](#mkinit) section and follw the next commands

### Exit from chroot
```bash
exit
umount -a
reboot
```

