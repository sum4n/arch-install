#!/bin/bash

# =================================================== ::
#
# Author       ::sum4n
# Source       ::https://gitlab.com/sum4n/bin
# Maintainer   ::to_suman@outlook.com
# Project      ::arch linux installation script
#
# =================================================== ::

# ask timezone
while [ "$zone" != "1" ] && [ "$zone" != "2" ]; do
  clear;printf "\n>> Setup Timezone\n\n"
  printf "Make a choice
1. set timezone
2. list timezone\n"
  printf "?# ";read -r zone
  if [ "$zone" = "2" ] || [ "$zone" = "1" ]; then
    if [ "$zone" = "1" ]; then
      break
    else
      find /usr/share/zoneinfo/*/* | awk -F "/" '{print $5"/"$6}' | less
    fi
  fi
done
sleep 2

# set timezone
while true; do
  clear;printf "\nName your timezone (Zone/Subzone): ";read -r _zone
  if [ -z "$_zone" ]; then
    break
  else
    ln -sf /usr/share/zoneinfo/"${zone[*]}" /etc/localtime
    hwclock --systohc
    break
  fi
done
sleep 2


# ask locale
ask_locale()
{
  while [ "$loc" != "1" ] && [ "$loc" != "2" ]; do
    clear;printf "\n>> Setup Locale\n\n"
    printf "Make a choice
1. set a locale
2. list locale\n"
    printf "?# ";read -r loc
    if [ "$loc" = "1" ] || [ "$loc" = "2" ]; then
      if [ "$loc" = "1" ]; then
        break
      else
        less /etc/locale.gen
      fi
    fi
  done
}

# set locale
set_locale()
{
  printf "\nName your locale (en_US.UTF-8): " && read -r _loc
  [ -z "$_loc" ] && { echo "en_US.UTF-8" >> /etc/locale.gen; echo "LANG=en_US.UTF-8" >> /etc/locale.conf; }
  [ -n "$_loc" ] && { echo "${_loc[*]}" >> /etc/locale.gen; echo "LANG=${_loc[*]}" >> /etc/locale.conf; locale-gen; }
}

ask_locale
sleep 2;clear
set_locale
sleep 2;clear

# ask keymap
while [ "$key" != "1" ] && [ "$key" != "2" ]; do
  clear;printf "\n>> Setup Keymap\n\n"
  printf "Make a choice
1. set keymap
2. list keymap\n"
  read -rp "?# " key
  if [ "$key" = "1" ] || [ "$key" = "2" ]; then
    if [ "$key" = "1" ]; then
      break
    else
      find /usr/share/kbd/keymaps/**/*.map.gz | awk -F "/" '{print $NF}' | less
    fi
  fi
done
sleep 2

# load keymap
clear;printf "\nName your keymap (us): ";read -r _key
[ -z "$_key" ] && echo "KEYMAP=us" >> /etc/vconsole.conf || echo "KEYMAP=${_key[*]}" >> /etc/vconsole.conf
sleep 2

# hostname and hosts file
while [ -z "$host" ]; do
  clear;printf "\nCreate a hostname: ";read -r host
done
echo "${host[*]}" > /etc/hostname
{ echo "127.0.0.1    localhost"; \
  echo "::1          localhost"; \
  echo "127.0.1.1    arch.localdomain    arch"; } >> /etc/hosts
sleep 2

# create user and password
clear;printf "\n>> Setup root and user password\n"
passwd && sleep 1
while [ -z "$usr" ]; do
  clear;printf "\nCreate a username: " && read -r usr
done
useradd -mG wheel,optical,storage,audio,video "${usr[*]}"
usermod -aG "${usr[*]}" "${usr[*]}"
passwd "${usr[*]}";sleep 1
echo "${usr[*]} ALL=(ALL) ALL" >> /etc/sudoers.d/"${usr[*]}";sleep 2

# grub and grub related
clear;printf "\n>> Grub setup\n\n"
printf "You want DualBoot system [y/n]: "; read -r dualboot
pacman -S --noconfirm --needed grub efibootmgr mtools dosfstools
[ "$dualboot" = "y" ] && pacman -S os-prober --noconfirm --needed
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub_uefi --recheck # change the directory to /boot/efi is you mounted the EFI partition at /boot/efi
grub-mkconfig -o /boot/grub/grub.cfg
sleep 2

# install driver packages
while [ "$cpu" != "1" ] && [ "$cpu" != "2" ]; do
  clear;printf "\n>> Install all gpu and driver packages\n\n"
  printf "What is your CPU
1. AMD
2. INTEL\n"
  printf "?# "; read -r cpu
done
[ "$cpu" = "1" ] && pacman -S --noconfirm --needed xf86-video-amdgpu amd-ucode
[ "$cpu" = "2" ] && pacman -S --noconfirm xf86-video-intel intel-ucode
sleep 2

# gpu driver
clear;printf "\nYou have nvidia drivers [y/n]: "; read -r gpu
[ "$gpu" = "y" ] && pacman -S --noconfirm nvidia nvidia-lts nvidia-utils nvidia-settings
[ "$gpu" = "n" ] && printf "No driver detected!!\n"
sleep 2

pacman -S --noconfirm --needed - < network-pkgs.txt # install network packages
pacman -S --noconfirm --needed - < tool-pkgs.txt # other tool packages
pacman -S --noconfirm --needed - < driver-pkgs.txt
sleep 2

# desktop install
while true; do
  clear; printf "\nYou want to install xfce4-desktop [y/n]: "; read -r _xfc; printf "\n"
  if [ "$_xfc" = "y" ]; then
    mkdir -p /usr/share/themes/empty/xfwm4/ && touch /usr/share/themes/empty/xfwm4/themerc # theme for remove border and title bar
    pacman -S --noconfirm --needed - < desktop-pkgs.txt
    break
  elif [ "$_xfc" = "n" ]; then
    printf "[+] Continue: to next..\n"
    break
  else
    continue
  fi
done
sleep 2

# enable systemctl services
clear; printf "\nEnable systemctl services\n\n"
systemctl enable NetworkManager
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable acpid
systemctl enable upower

# final steps
printf "\nDone! Type these three commands
exit
umount -a
reboot\n"
sleep 2
